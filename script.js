document.addEventListener('DOMContentLoaded', function() {
    const gamesContainer = document.getElementById('gamesContainer');

    fetch('http://127.0.0.1:8080/games')
        .then(response => response.json())
        .then(data => {
            data.games.forEach(game => {
                const gameDiv = document.createElement('div');
                gameDiv.className = 'game-item';
                gameDiv.innerHTML = `
                    <strong>Game ID:</strong> ${game.game_id}<br>
                    <strong>Number of attempts:</strong> ${game.attempts}<br>
                    <strong>Players:</strong> ${game.players}<br>
                    <strong>Status:</strong> ${game.status}<br>
                `;
                gamesContainer.appendChild(gameDiv);
            });
        })
        .catch(error => {
            console.error('Failed to fetch games:', error);
            gamesContainer.innerHTML = '<p>Error loading games.</p>';
        });
});
