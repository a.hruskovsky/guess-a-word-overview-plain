# Guess a word overview plain

## Name
Guess a word overview plain

## Description
Project is written in plain HTML + CSS + JS

## Usage
Overview of all games and their status. When server is running (from Guess a word project here https://gitlab.com/a.hruskovsky/guess-a-word) it automatically listen on port 8080. So when server is running just open index.html in you web browser.
